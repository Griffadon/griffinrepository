﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingscript : MonoBehaviour
{
    public float speed = 5.0f; //controls the speed the characters moves

    private Vector3 input; //Tracks pressed inputs as numbers
    private Vector3 motion; //Tracks movement step per frame
    private CharacterController controller;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        motion = Vector3.zero;
        input = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Depth"), Input.GetAxis("Vertical"));
        motion += transform.forward.normalized * input.z;
        motion += transform.right.normalized * input.x;
        motion += Vector3.up.normalized * input.y;
        controller.Move(motion * speed * Time.deltaTime);
    }
}
