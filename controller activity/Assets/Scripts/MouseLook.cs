﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float sensitivity = 2.5f;
    public float drag = 1.5f;
   

    private Transform character;
    private Vector2 mouseDir;
    private Vector2 smoothing;
    private Vector2 result;


    //Best place to make component references before usings components
    //Awake is called before Start
    private void Awake()
    {
        character = transform.parent;
        //Make reference to required character controller component

    }

    private void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mouseDir = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")) * sensitivity;
        smoothing = Vector2.Lerp(smoothing, mouseDir, 1 / drag);
        result += smoothing;
        result.y = Mathf.Clamp(result.y, -70, 70);


        //Rotate the camera
        transform.localRotation = Quaternion.AngleAxis(-result.y, Vector3.right);
        //Rotate the charater
        character.rotation = Quaternion.AngleAxis(result.x, character.transform.up);
    }
}
